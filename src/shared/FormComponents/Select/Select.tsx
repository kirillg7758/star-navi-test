import React, { FC, useRef } from "react";

import { useVisible } from "../../hooks/useVisible";
import { IMode } from "../../../store/reducers/tileReducer/tileReducer";
import downArrow from "../../../assets/icons/down-arrow.svg";
import "./Select.scss";

interface ISelectProps {
    options: IMode[],
    title: string,
    setTitle: (mode: IMode) => void
}

const Select: FC<ISelectProps> = ({ options, title, setTitle }) => {
    const selectRef = useRef(null)
    const { isVisible, setIsVisible } = useVisible(false, selectRef)
    const showDropDownClass = isVisible ? "show" : ""
    return (
        <div className="select" ref={selectRef}>
            <div className="select__head" onClick={() => setIsVisible(!isVisible)}>
                <h1 className="select__head--title">{title}</h1>
                <img
                    className={"select__head--icon " + showDropDownClass}
                    src={downArrow}
                    alt="#"
                />
            </div>
            <div className={"select__options " + showDropDownClass}>
                {options.map((item, idx) =>
                    <span
                        key={"option-" + idx}
                        className="select__options--item"
                        onClick={() => {
                            setTitle(item)
                            setIsVisible(false)
                        }}
                    >{item.name}</span>)}
            </div>
        </div>
    )
}

export default Select
