import React, { FC } from "react";
import { applyMiddleware, combineReducers, createStore } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";

import { tileReducer } from "./reducers/tileReducer/tileReducer";

const rootReducer = combineReducers({
    tile: tileReducer
})

const store = createStore(rootReducer, applyMiddleware(thunk))

export type RootType = ReturnType<typeof rootReducer>

export const StoreProvider: FC = ({ children }) => {
    return (
        <Provider store={store}>
            {children}
        </Provider>
    )
}
