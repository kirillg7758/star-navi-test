export enum TileTypes {
    GET_MODES = "GET_MODES",
    TOGGLE_TILE = "TOGGLE_TILE",
    CLEAR_TILES = "CLEAR_TILES"
}

export type ITileAction = {
    type: TileTypes.GET_MODES,
    payload: ITileState["modes"]
} | {
    type: TileTypes.TOGGLE_TILE,
    payload: ITile
} | {
    type: TileTypes.CLEAR_TILES
}

export interface IMode {
    name: string,
    field: number
}

export interface ITile {
    id: number,
    col: number,
    row: number
}

export interface ITileState {
    modes: IMode[],
    tiles: ITile[]
}

const init: ITileState = {
    modes: [{ name: "Pick Mode", field: 0 }],
    tiles: []
}

export const tileReducer = (state = init, action: ITileAction) => {
    switch (action.type) {
        case TileTypes.GET_MODES:
            return {
                ...state,
                modes: [...state.modes, ...action.payload]
            }
        case TileTypes.TOGGLE_TILE:
            const tile = state.tiles.find(elem => elem.id === action.payload.id)
            if(tile) {
                const filterTiles = state.tiles.filter(elem => elem.id !== tile.id)
                return {
                    ...state,
                    tiles: [...filterTiles]
                }
            } else {
                return {
                    ...state,
                    tiles: [...state.tiles, action.payload]
                }
            }
        case TileTypes.CLEAR_TILES:
            return {
                ...state,
                tiles: []
            }
        default:
            return state
    }
}
