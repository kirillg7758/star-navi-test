import React, { FC, useEffect, useState } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";

import Select from "../../../shared/FormComponents/Select";
import Button from "../../../shared/FormComponents/Button";

import { IMode, ITile, TileTypes } from "../../../store/reducers/tileReducer/tileReducer";
import { getTiles } from "../../../store/reducers/tileReducer/actions/getTiles";
import { RootType } from "../../../store/store";

import GridTable from "../../components/GridTable";
import InfoList from "../../components/InfoList";

const MainPage: FC = () => {
    const [mode, setMode] = useState<IMode>({ name: "Pick Mode", field: 0 })
    const [isStart, setIsStart] = useState<boolean>(false)
    const { modes } = useSelector(({ tile }: RootType) => tile, shallowEqual)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getTiles())
    }, [dispatch])

    const onMouseEnter = (tile: ITile) => {
        dispatch({
            type: TileTypes.TOGGLE_TILE,
            payload: tile
        })
    }

    return (
        <div style={{
            maxWidth: '1000px',
            margin: '3rem auto 0',
            display: "flex",
            justifyContent: "flex-start",
            alignItems: "flex-start"
        }}>
            <div style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                justifyContent: "center",
                height: "100%",
                padding: "0 0 3rem"
            }}>
                <div style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between"
                }}>
                    <Select
                        title={mode.name}
                        options={modes}
                        setTitle={(mode) => {
                            setMode(mode)
                            setIsStart(false)
                            dispatch({
                                type: TileTypes.CLEAR_TILES
                            })
                        }}
                    />
                    <Button
                        style={{ marginLeft: "10px" }}
                        type="button"
                        value="Start"
                        onClick={() => setIsStart(true)}
                    />
                </div>
                <GridTable
                    fieldSquare={isStart ? mode.field : 0}
                    onMouseEnter={onMouseEnter}
                />
            </div>
            <InfoList />
        </div>
    )
}

export default MainPage
